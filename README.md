# Ashurbanipal

Main code of a bulk publisher of information to OIP

> This documentation needs to be updated

## Requirements

fcoin node

## User API


```bash
ashurbanipal --config myConfig.json --mode pretend --records records1.csv records2.csv ... 
ashurbanipal --config myConfig.json --mode publish --records records1.csv records2.csv ...
```

## Input Format

Ashurbanipal takes a series of `csv` files with headers. All files must contain a `localId` column with unique keys.

The other columns headers is the `template` code and `field`. For example:
the values in the column `tmpl_20AD45E7.name` will be associated with the field `name` of the template `tmpl_20AD45E7` (the basic template of DDX).

## Config file

The configuration file is a json formatted file with the following structure:

```typescript
{
    broadcaster: { // config related to the blockchain node used for publication
        network: "mainnet", // type of the network
        rpc: { // rpc configuration of your node
            port: 7313, 
            host: "127.0.0.1",
            username: "username",
            password: "password"
        },
        publicAddress: "FFb1em1ZBE6BzLihSCxscqW6KLkLtpWzBH", // public address of broadcaster
        wif: "RAh4VD17ckTAqHRQ6QXpYBUXL3UGakoubyeB3unuTzvPBEnGZJ1A", // wif of broadcaster
        txFeePerByte: 0.0001 / 1024 // fees per bytes (increase if want high priority in the blocks)
    },
    owner: { // config in case owner is different from broadcaster. Copy broadcaster config info if the same.
        wif:"yourWifHere",
        publicAddress: "ownerPublicAddress"
    }
}
```

## Restart

If there is a loss of connectivity or interruption, we can restart Ashurbanipal by rerunning the command.

Ashurbanipal will load the local databases with existing records and skip publishing records already in the `localId-oipRef` DB. Everything else will work as expected until completion.

Any change in the config file passed to Ashurbanipal will be seen as a different submission, and it will start publishing records from scratch.

If we want to force a clean start with the same config file, we must pass the obviously dangerous flag `--start-over-from-scratch-i-know-what-i-am-doing`. Note that this might re-publish records which will receive a different `oipRef`. The records will be exactly the same but with another identifier and a separate block in the chain.

Be careful to use this flag because we currently do not have an automated solution to deactivate records in bulk.

## Mode: pretend and publish

There are two modes in `ashurbanipal`: `pretend` and `publish`. The pretend mode simulates the publication of the dataset. It is a valuable mode to verify if there are mistakes with referencing other records or circular references. In `pretend` mode, the database is erased when the publication of the entire dataset is over.

When you are sure there are no mistakes in the dataset, we can use the `publish` mode. Here, the internal database of published records is never erased unless requested (see [Restart](#restart)).

## Dev notes


## Reference to other records

Perhaps the most powerful feature of OIP/PIN is the re-use of metadata in other records by their oipRef identifier. However, these identifiers are issued in the process of publishing the record on the PIN blockchain.

Ashurbanipal's engine lets us reference other records before the publication to the blockchain.

For this reason, a mandatory field is the `localId` column. This column must have identifiers (keys) for each line (record) about to be published. They do not need to be unique across all the `csv` files submitted to Ashurbanipal, but they must be unique within each file.

To reference a record by its `localId` we just need to add a prefix `localId-` to the local id. For example:

Say record with `localId: 0002` references record with `localId: 0001`. The csv file would look like something like this:

|localId|field1|
|-|-|
|0001||
|0002|localId-0001|

If we want to reference a record in another csv file, we need to, instead, use the `ashuRef` and the name of the file without the `.csv` as prefix. All separated by `-`. For example, let's suppose that the records we just described before are from the `file1.csv` and now we have a new `csv` file named `file2.csv` with one record referencing the second record in `file1.csv`

|localId|field1|
|-|-|
|0001|ashuRef-file1-0002|

> It is important to note that localIds are treated as `string`s and not `number`s. Thus, the number of zeros (if any) should match the `localId` column.

Ashurbanipal will try to interpret these references across all the files submitted and re-organize the order of publication to only publish records with references when the referred record has been already published and received their unique `oipRef`. Ashurbanipal will substitute the localId-based identifiers by the corresponding `oipRef`.

Prior to start the publication of the records, Ashurbanipal will try to make sure there is no circular references and organize the publication order to guarantee correct publication.

The API has a `--pretend` option that executes the entire process without publishing to the chain and should be used to debug the dataset. Ashurbanipal's try to be explicit of which records are in conflict and will point out 1 problem at the time.


### Graph

One of the outputs of Ashurbanipal is the complete graph of dependencies generated.

### Main API

```typescript
import csv2json from 'csv2json'
import { Ashurbanipal } from 'ashurbanipal'

const config = {
    broadcast: {
        network: "main",
        rpc: {
            port: 7313,
            host: "127.0.0.1",
            username: "username",
            password: "password"
        },
        publicAddress: env.BC_PUB_ADDR,
        wif: env.BC_WIF
    },
    ownership: {
        mnemonic: env.OWN_MNEMONIC
        publicAddress: env.OWN_PUB_ADDR
    }
}

const ash = new Ashurbanipal(config)
ash.init().then(() => {
    ash.loadCsvFiles(['file1.csv', 'file2.csv'])
    ash.publish()

```

### Scheduler logic

#### Using cytoscape

By using graph theory the algorithm becomes simple.

We will build a graph of the records. Records with dependencies will be the target of directed edges pointing from parent.

When building the graph, Ashurbanipal's logic is:

1) Add node to graph if not there yet.

2) update metatdata content with already published info

3) add updated metadata to the node

4) check for pending dependencies.

    4.1) Add pending dependencies as nodes to the graph.

5) if there is no pending dependencies, publish it.

6) get oipRef and search for all dependents in graph

7) update record metadata and put each dependent through step 4.


The structure of the data in the node is:

```javascript
{
    group: 'nodes'
    data: {
        localId,
        oipRef,
        recordData
    }
}
```

The data structure of the edge is:

```javascript
{
    group: 'edges',
    data: {
        source,
        target
    }
}
```

record -> graph <-> publication


## To do

* Vizualization of graph indicating graph with problems (oipRef attribute should not be `undefined`)


#### Older tentative algorithms
1) check if record has been published, if it has, skip
2) check if record has any dependencies and if not publish it and add to the `publDB` DB.
3) check in the `publDB` if the dependencies have been published and if it has, sub the `localId`s to the correspondent `oipRef`s and publishe it.
4) check if the dependency is in the `depeDB` database and if it is not, add it with the record `localId` in the field: `lo` (locks). 
5) check if the record is on `depeDB` and if not, add an item to the `depeDB` of the record itself with the dependency as `loBy` (locked by).
6) store the record on `waitDB`
7) if dependency is already in the `depeDB`, add the record's `localId` to the `lo` array.
8) if record is already in the `depeDB`, add the dependency's `localId` to the `loBy` array.
9) move to next record.

When a record is published:
1) add record to `publDB`
2) load its record on `depeDB` and check for dependents on `lo`.

3) load each dependent in `lo` and remove the record's `localId` from their `loBy` array.
4) if `loBy` of dependent is empty, schedule it for publishing (this, right here is memory caution)
5) if not, remove the record from the dependent's `loBy` and update `depeDB`.

Before reading a new record, drain the scheduled array.


### Scheduler Database infrastructure

Ashurbanipal has 3 PouchDB databases: `depeDB`, `waitDB`, and `publDB`. 

The `depeDB` keeps track of the record dependencies with the following interface:

```json
{
    _id: string,
    locks: string[],
    loBy: string[] 
}
```

This DB gets updated all the time.

The `waitDB` is the waiting list. It keeps the record body waiting to be published.

The last one, the `publDB` keeps track of the already published records and associate the `localId` with the `oipRef`. It is the reference used to decide if a record has been published or not.


## Formating the records

Columns:
|Column|description|
|-|-|
|localId| A unique identifier for each row. It can be used as a cross reference. Ashurbanipal will wait until all dependent records have been resolved and switch the `localId` by its `oipRef`.|

### Empty data fields

Empty data fields are considered NA (non-applicable) and removed from the record. Unless the field is an array type, in that case, Ashurbanipal will replace it with an empty array.

## Helpers
`.verifyDataset(input.oip.csv)`

Verify broken links and if a publishing tree is possible to be designed.

## Latest changes
