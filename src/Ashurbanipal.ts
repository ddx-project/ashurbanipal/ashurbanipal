import fs from 'fs'
import crypto from 'crypto'
import { Readable } from 'stream'
import Pumpify from 'pumpify'
import csv from 'csvtojson'
import { IConfigAshurbanipal, IDbSchema, IPublDB, ISource, IStats } from './interfaces'
// import { Publisher } from './Publisher'
import { UpdateLocalIdTransform } from './UpdateLocalIdTransform'
import { Logger, LogLevelDescType } from 'loglevel-colored-prefix'
import { Scheduler } from './Scheduler'
import { lowerFirst } from 'lodash'

/*
Cascade =>
1) parse csv into json using csvtojson
2) json record to OIP record
3) broadcast
*/



class Ashurbanipal {
    private stats: IStats
    private readonly config: IConfigAshurbanipal
    private readonly loglevel: LogLevelDescType
    private readonly log: Logger
    private readonly isTest: boolean
    private readonly eraseDB: boolean
    private _datasetSig: string
    private _scheduler: Scheduler | undefined
    private _sources: ISource[]

    constructor(
        config: IConfigAshurbanipal, 
        loglevel: LogLevelDescType = "info",  
        isTest: boolean = false,
        eraseDB: boolean = false
    ) {
        this.config = config
        this._sources = []
        this._scheduler = undefined
        this.loglevel = loglevel
        this.isTest = isTest
        this.eraseDB = eraseDB
        this._datasetSig = ""        
        this.log = new Logger(loglevel)
        this.stats = {
            published: 0,
            skipped: 0
        }
    }

    public get datasetSig(): string {
        return this._datasetSig
    }

    private set scheduler(pub: Scheduler) {
        this._scheduler = pub
    }

    private get scheduler(): Scheduler {
        if (this._scheduler === undefined) {
            throw Error('`Ashurbanipal has not being initialized')
        }
        return this._scheduler
    }

    public exportDb(): IPublDB[] | undefined {
        if (this.scheduler.db) {
            return this.scheduler.db.get('publDB').value()
        }
    }

    public async init() {
        const log = this.log.getLogger("Ashurbanipal::init")
        if (this._sources.length === 0) {
            log.error('Must load data before initialize it')
            throw new Error('Must load data before initialize it')
        }
        this._datasetSig = await this.calcSigs()
        log.debug(`sig: ${this.datasetSig}`)
        const scheduler = new Scheduler(this.config, this.loglevel, this.isTest)
        await scheduler.init(this.datasetSig, this.eraseDB)
        this.scheduler = scheduler
    }
    
    public getStats() {
        return this.stats
    }
    
    public loadCsvFiles(fileNameArrays: string[]): this {
        const log = this.log.getLogger("Ashurbanipal::loadCsvFiles")
        this._sources = fileNameArrays.map(filename => {
            const name = this.parseFilename(filename)
            return {
                sigStream: fs.createReadStream(filename),
                stream: fs.createReadStream(filename),
                name
            }
        })
        log.debug('Done')
        return this
    }

    public preProcess(name: string): Pumpify {
        const updateLocalId = new UpdateLocalIdTransform(name, this.loglevel)
        const preProcess = new Pumpify.obj([
            csv(),
            updateLocalId
        ])
        preProcess.on('error', (err) => {
            throw err
        })
        return preProcess
    }

    public async publish(pretend = false, limit: number = 0): Promise<this> {
        const log = this.log.getLogger("Ashurbanipal::publish")
        log.info(`mode: ${pretend ? "pretend" : "publish"}`)
        const self = this
        return new Promise((resolve, reject) => {
            if (this._sources.length === 0) {
                const errMsg = "There are no more data source. If you are trying to publish more records, you must start a new instance and load the files again."
                log.error(errMsg)
                reject(errMsg)
            }
            this.scheduler.pretend = pretend
            this.scheduler.limit = limit
            while (this._sources.length > 1) {
                const source = this._sources.shift()
                source?.stream
                    .pipe(this.preProcess(source.name), {end: false})
                    .pipe(this.scheduler)
            }
            const source = this._sources.shift()
            const pipeline = source?.stream
                .pipe(this.preProcess(source.name))
                .pipe(this.scheduler)
                .on('finish', () => {
                    this.stats = this.scheduler.getStats()
                })
            
            this.scheduler.on('data', () => {
                this.stats = this.scheduler.getStats()
            })
            
            pipeline?.on('error', (err: Error) => {
                log.error(err)
                reject(err)
            })
    
            pipeline?.on('finish', () => {
                log.info(`Done`)
                resolve(self)
            })
        })
    }

    protected parseFilename(filename: string) {
        const pathParts = filename.split(/\/|\\/)
        const nameWithExtension = pathParts.pop()
        if (!nameWithExtension) {
            throw Error(`Invalid Filename: ${filename}`)
        }
        const nameParts = nameWithExtension.split('.')
        nameParts.pop()
        return nameParts.join('.')
    }

    protected async calcSigs(): Promise<string> {
        const log = this.log.getLogger("Ashurbanipal::calcSigs")
        const sigs: string[] = []
        for (const source of this._sources) {
            log.debug(`Calc sigs from: ${source.name}`)
            const sig = await this.calcSig(source.sigStream)
            sigs.push(sig)
        }
        sigs.sort()
        const allSigs = sigs.join('')
        return crypto
            .createHash('sha256')
            .update(allSigs)
            .digest('hex')
            .slice(0, 6)
    }

    protected async calcSig(stream: Readable): Promise<string> {
        const log = this.log.getLogger("Ashurbanipal::calcSig")
        const hash = crypto.createHash('sha256')
        hash.setEncoding('hex')
        log.debug('starting the calculation of a hash')
        return new Promise((resolve, reject) => {
            stream.pipe(hash)
                .on('finish', () => {
                    hash.end()
                    resolve(hash.read())
                })
                .on('error', (err) => {
                    log.error(err)
                    reject(err)
                })
        })
    }
}

export {
    Ashurbanipal
}