import axios from "axios";
import { Logger, LogLevelDescType } from "loglevel-colored-prefix";
const { buildOipDetails } = require("oip-protobufjs/lib/builders");

import { oipApiBaseUrl } from "./definitions";
import { IFileDescriptor, IOipDetails } from "./interfaces"

class ParseRecords {
    private fileDescriptors: IFileDescriptor[]
    private readonly log: Logger
    private readonly logLevel: LogLevelDescType

    constructor(logLevel: LogLevelDescType) {
        this.fileDescriptors = []
        this.logLevel = logLevel
        this.log = new Logger(logLevel)
    }

    public async encode(recordData: any): Promise<IOipDetails[]> {
        const log = this.log.getLogger(`ParseRecords::encode`)

        const allRecordDetails = []
        for (const templateName in recordData) {
            if (templateName.match(`tmpl`)) {
                const template = await this.getTemplate(templateName)
                log.debug(template)
                const details: IOipDetails = {
                    name: templateName,
                    descriptor: template.descriptor,
                    payload: recordData[templateName]
                }

                allRecordDetails.push(details)
            }
        }
        // const allDetails = buildOipDetails(allRecordDetails) as string[]
        return allRecordDetails
    }

    protected async getTemplate(templateId: string) {
        const log = this.log.getLogger("OipApi::getTemplates");
        let descriptor = this.fileDescriptors.find(d => d.name === templateId)
        if (descriptor) {
            return descriptor    
        }
        const id = templateId.replace("tmpl_", "");
        const url = `${oipApiBaseUrl}template/get/${id}`;
        log.debug(url)
        const template = await axios.get(url) //.then(res => res.data);
        const fileDescriptor =
        template.data.results[0].template.file_descriptor_set;
        template.data.descriptor = fileDescriptor;
        this.fileDescriptors.push({
            name: templateId,
            descriptor: template.data
        })
        const extended = template.data.results[0].template.extends;
        if (extended) {
            for (const templateIdExtended of extended) {
                const extendedCode = `tmpl_${templateIdExtended.toString()}`;
                if (!this.fileDescriptors.find(d => d.name === extendedCode)) {
                    await this.getTemplate(extendedCode)
                }
            }
        }
        return template.data;
    }

}

export { ParseRecords }