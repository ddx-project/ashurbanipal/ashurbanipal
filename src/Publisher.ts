// import fs from 'fs'
import * as bip39 from 'bip39'
import * as bip32 from 'bip32';
import bs58check from 'bs58check';
import wif from 'wif'

// import WebSocket from 'ws'
import createHash from 'create-hash'
import { BIP32Interface } from 'bip32';
import { LogLevelDescType, Logger} from 'loglevel-colored-prefix'

// import { floNetwork } from './flo.network'
//import bs58check from 'bs58check';
//import { NetworkInterfaceBase } from 'os';
//import { IFormatedUtxo, IParamsBuildInAndOut, IRawTx, IUnspent, IVout } from './interfaces';
//import { log } from 'console';

import * as btc from 'bitcoinjs-lib'
import { IConfigAshurbanipal, IConfigBroadcaster, IConfigOwner } from './interfaces';
import { createReadStream } from 'fs';
// import { PsbtInput, PsbtInputExtended, PsbtOutputExtended } from 'bip174/src/lib/interfaces';

// const { OIP } = require('js-oip')
const { Wallet, Address, Networks, FloPsbt } = require('@oipwg/hdmw')

const { RPCWallet, ExplorerWallet } = require('js-oip/lib/modules/wallets')
const { MultipartX } = require('js-oip/lib/modules/multipart')
// const { FLOTransactionBuilder, FLOTransaction } = require('js-oip/lib/modules/flo')

const coinselect = require('coinselect')
// const {NodeClient, WalletClient } = require('fclient')
// import * as flo2 from '@oipwg/bitcoinjs-lib'

const FLODATA_MAX_LEN = 1040
const SAT_PER_FLO = 100000000
const user = "I3uZ1Jsg2t";
const password = "dCIb5114Zm";

const ripemd160 = (buffer: Buffer): Buffer => {
    return createHash('rmd160').update(buffer).digest()
}

const sha256 = (buffer: Buffer): Buffer => {
    return createHash('sha256').update(buffer).digest()
}

const hash160 = (buffer: Buffer): Buffer => {
    return ripemd160(sha256(buffer))
}

interface IDeriveAll {
    masterNode: BIP32Interface
    node: BIP32Interface
    purposeNode: BIP32Interface
    coinNode: BIP32Interface
    accountMaster: BIP32Interface
    external: BIP32Interface
    address: BIP32Interface
    pubAddr: string
}

// const cert = fs.readFileSync('/home/px44b/.flod/rpc.cert')

const floFeePerKb = 0.00015 * SAT_PER_FLO

const floConfig = {
    name: 'flo',
    displayName: 'Flo',
    ticker: 'FLO',
    satPerCoin: 1e8,
    feePerKb: floFeePerKb,
    feePerByte: floFeePerKb / 1024,
    maxFeePerByte: 100,
    minFee: floFeePerKb,
    dust: 100000,
  
    txVersion: 2,
    hasFloData: true,
  
    network: {
      bip32: {
        public: 0x0134406b,
        private: 0x01343c31
      },
      slip44: 216,
      messagePrefix: '\u001bFlorincoin Signed Message:\n',
      pubKeyHash: 35,
      scriptHash: 94,
      wif: 163
    }
  }


class Publisher {
    private readonly config: IConfigAshurbanipal
    private readonly log: Logger
    private readonly logLevel: LogLevelDescType
    private ready: boolean
    private address: any
    private wallet: typeof RPCWallet | undefined
    private owner: IConfigOwner | undefined
    private broadcaster: IConfigBroadcaster | undefined

    static readonly FEE = 1

    constructor(config: IConfigAshurbanipal, logLevel: LogLevelDescType = "debug") {
        this.logLevel = logLevel
        this.config = config
        this.log = new Logger(logLevel)
        this.address = undefined
        this.wallet = undefined
        this.owner = undefined
        this.broadcaster = undefined
        this.ready = false
    }


    public async init() {
        await this.configBroadcaster(this.config.broadcaster)
        await this.configOwner(this.config.owner)
        this.ready = true
        return this
    }

    public async configBroadcaster(broadcastConfig: IConfigBroadcaster) {
        this.broadcaster = broadcastConfig
        if (!this.wallet) {
            this.wallet = new RPCWallet(broadcastConfig)
            await this.wallet.initialize()
            // console.log(this.wallet.wif)
        }
        this.broadcaster = broadcastConfig
    }

    public async configOwner(ownerConfig: IConfigOwner) {
        this.owner = ownerConfig
    }

    public isReady(): boolean {
        return this.ready
    }

    public fakeFeedBlockchain(oipprotobuff: string) {
        const fakeTxid = createHash('sha256')
        fakeTxid.update(oipprotobuff)
        return fakeTxid.digest('hex')
    }

    public async feedBlockchain(oipprotobuff: string) {
        const log = this.log.getLogger(`Publisher::feedBlockchain`)
        if (!this.ready) {
            throw Error("Must config it first")
        }

        const wif = this.owner?.wif || this.broadcaster?.wif

        if (!wif) {
            log.error('No wif')
            throw new Error('No wif')
        }

        const ownerData = {...this.broadcaster, ...this.owner}
        log.debug(ownerData)
        const ownerWallet = new RPCWallet(ownerData) // btc.ECPair.fromWIF(wif, Networks.flo.network)

        const dataChunks = []
        const txids = []

        if (oipprotobuff.length > FLODATA_MAX_LEN) {
            const mpx = new MultipartX(oipprotobuff)
            const mps = mpx.getMultiparts()
            for (const mp of mps) {
                mp.setAddress(this.owner?.publicAddress)
                mp.setReference(txids[0])
                let { error } = await mp.signSelf(ownerWallet.signMessage.bind(ownerWallet))
                if (error) {
                    log.error(error)
                    throw new Error(error)
                }
                log.debug(mp)
                log.debug(mp.toString())
                if (!mp.isValid().success) {
                    console.log(mp)
                    throw new Error(`Invalid multipart: ${mp.isValid().error}`)
                }

                const txid = await this.sendDataToChain([mp.toString()])
                txids.push(txid[0])
                log.debug(txids)
             }
            log.debug(mps)
            return txids[0]
            // throw Error(`We do not accept data larger than 1 chunk`)
        } else {
            dataChunks.push(oipprotobuff)
            const ids = await this.sendDataToChain(dataChunks)
            return ids[0]
        }


    }

    private async sendDataToChain(data: string[]) {
        const log = this.log.getLogger(`Publisher::sendDataToChain`)
        if (!this.broadcaster) {
            throw Error('Broadcaster has no configuration')
        }
        const txIds = []
        for (let chunk of data) {
            log.info(`Trying to add ${chunk} to FLO blockchain`)
            try {
                await this.wallet.checkAllUnconfirmed()
                const utxos = await this.wallet.getUTXOs()
                const utxoTxs = utxos.map((u: { txid: string; }) => u.txid)
                const fullTxs = await this.wallet.rpcRequest('getrawtransaction', utxoTxs)
                const tx = new FloPsbt(floConfig)
                log.debug(tx)
                tx.setVersion(2)
                tx.setLocktime(0)
                const formattedUtxos = utxos.map((utxo: { address: any; txid: any; vout: any; scriptPubKey: any; amount: number; confirmations: any; }) => {
                    return {
                      address: utxo.address,
                      txId: utxo.txid,
                      vout: utxo.vout,
                      scriptPubKey: utxo.scriptPubKey,
                      value: Math.ceil(utxo.amount * floConfig.satPerCoin),
                      confirmations: utxo.confirmations
                    }
                })
                const totalValue = formattedUtxos.reduce((t: number, a: any) => t + a.value, 0) // * floConfig.satPerCoin
                log.debug(`txFeePerByte: ${this.broadcaster.txFeePerByte}`)
                log.debug(`chunk length: ${chunk.length}`)
                log.debug(`min fee: ${floConfig.minFee}`)
                const txfee = Math.ceil(Math.max(this.broadcaster.txFeePerByte * chunk.length * floConfig.satPerCoin, floConfig.minFee)) // * floConfig.satPerCoin
                log.info(`fees: ${txfee}`)
                const output = {
                    address: this.broadcaster.publicAddress,
                    value: totalValue - txfee
                }
                log.debug(output)
                log.debug(formattedUtxos)
                log.debug(`feeRate: ${Math.ceil(floConfig.feePerByte)}`)
                const selected = coinselect(formattedUtxos, [], Math.ceil(floConfig.feePerByte))
                log.debug(selected)
                selected.inputs.forEach((utxo: any, i: number) => {
                    tx.addInput({ hash: utxo.txId, index: utxo.vout, nonWitnessUtxo: Buffer.from(fullTxs.result, 'hex')})
                })
                tx.setFloData(Buffer.from(chunk))
                log.debug(`output`)
                log.debug(output)
                tx.addOutput(output)
                
                const pair = btc.ECPair.fromWIF(this.broadcaster.wif, Networks.flo.network)
                log.info(`ECPair: ${pair.privateKey?.toString('hex')}`)

                log.debug(tx.data.outputs)
                
                tx.signAllInputs(pair)
                tx.validateSignaturesOfInput(0);
                log.debug(tx)
                tx.finalizeAllInputs();
                const rawTx = tx.extractTransaction()
                const hex = rawTx.toHex()
                
                const result = await this.wallet.rpcRequest('sendrawtransaction', [ hex ])
                log.debug(`result`)
                log.debug(result)
                await this.wallet.addAncestor(hex)

                txIds.push(result.result)
                await new Promise((resolve, reject) => { setTimeout(() => { resolve(0) }, 200) })
            } catch (err) {
                log.error(err)
            }
        }
        log.info(txIds)
        return txIds
    }

}

export {
    Publisher
}