import { Transform } from 'stream'
import { IConfigAshurbanipal } from './interfaces'
import { Publisher } from './Publisher'

class PublisherStream extends Transform {
    private readonly publisher: Publisher

    constructor(publisher: Publisher) {
        super()
        if (publisher.isReady()) {
            this.publisher = publisher
        } else {
            throw Error(`Publisher must have been initialized first`)
        }
    }
}