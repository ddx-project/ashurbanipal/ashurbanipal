import fs from 'fs'
import lowdb from "lowdb";

import { Transform } from "stream";
import cytoscape from 'cytoscape';

import { LogLevelDescType, Logger} from 'loglevel-colored-prefix'

import { dbFilename, dbFilenameTest, dbPath, prefix } from "./definitions";
import { IConfigAshurbanipal, IDbSchema, IEdge, INode, IPublDB, IRecord, IStats } from "./interfaces";
import { Publisher } from "./Publisher";
import FileAsync from "lowdb/adapters/FileAsync";
import { ParseRecords } from './ParseRecords';
import { cleanRecord } from './utils';

const { recordProtoBuilder } = require("oip-protobufjs/lib/builders");

class Scheduler extends Transform {
    public db: lowdb.LowdbAsync<IDbSchema> | undefined;
    
    private _limit: number
    private _pretend: boolean

    private readonly config: IConfigAshurbanipal
    private readonly isTest: boolean
    private readonly log: Logger
    private readonly loglevel: LogLevelDescType
    private readonly matchReferences: string
    private readonly _graph: cytoscape.Core

    private stats: IStats

    private publisher: Publisher | undefined 
    private scheduled: string[]
    private ids: number


    constructor(config: IConfigAshurbanipal, loglevel: LogLevelDescType = 'info', isTest: boolean = false) {
        super({objectMode: true})
        this.config = config
        this.isTest = isTest
        this._pretend = false
        this.loglevel = loglevel
        this.log = new Logger(loglevel)
        this.ids = 0
        this.matchReferences = `/${prefix}.*?"/`
        this.scheduled = []
        this._graph = cytoscape({headless: true})
        this.stats = {
            published: 0,
            skipped: 0
        }
        this._limit = 0
    }

    public async init(sig: string, eraseDB: boolean = false) {
        const log = this.log.getLogger("Scheduler::connect")
        log.info(`Connecting with DBs and preparing publisher`)
        const publisher = new Publisher(this.config, this.loglevel)
        if (!fs.existsSync(dbPath)) {
            fs.mkdirSync(dbPath);
        }
        const dbFile = this.isTest ? dbFilenameTest(sig) : dbFilename(sig)
        if (eraseDB && fs.existsSync(dbFile)) {
            log.warn("This DB will be erased")
            fs.unlinkSync(dbFile)
        }
        const adapter = new FileAsync<IDbSchema>(dbFile)
        const setups: [
            Promise<lowdb.LowdbAsync<IDbSchema>>, 
            Promise<Publisher>
        ] = [
            lowdb(adapter),
            publisher.init()
        ]

        const promises = await Promise.all(setups)
        this.db = promises[0]
        await this.db.defaults({
            publDB: []
        }).write()
        this.publisher = promises[1]
    }

    public getStats() {
        return this.stats
    }

    public set limit(limit: number) {
        this._limit = limit
    }

    public get limit() {
        return this._limit
    }

    public set pretend(pretend: boolean) {
        this._pretend = pretend
    }

    public get pretend(): boolean {
        return this._pretend
    }

    public get graph(): object {
        return this._graph.json()
    }

    public async _transform(record: IRecord, encoding: string, next: (error?: Error) => void) {
        const log = this.log.getLogger("Scheduler::_transform")
        if (this.limit !== 0) {
            log.info(`record: ${record.localId}`)
            log.info(`limit: ${this.limit}`)
            log.info(`stats: ${JSON.stringify(this.stats)}`)
            if (this.limit <= this.stats.published) {
                log.warn('Reached the limit.')
                return
            }
        }
        try {
            await this.schedulePipeline(record)
            next()
        } catch (err: any) {
            next(err)
            return
        }
    }

    public _flush(next: (error?: Error) => void) {
        const log = this.log.getLogger("Scheduler::_flush")
        log.debug("Flushing")
        try {
            const rest = this._graph.nodes().jsons().filter((rec: any) => {
                return rec.data.oipRef === undefined ? rec : undefined
            }).map((d: any) => d.data.id)
            if (rest.length > 0) {
                if (this.limit !== 0 && this.limit === this.stats.published) {
                    log.info('Published limit reached, but there are more records to be published. Re-run the command to keep publishing.')
                    next()
                    return
                }
                const err = Error(`The following records cannot be published: ${rest.join(', ')}`) ///.map((r: any) => r.id)}`)
                next(err)
            }
        } catch (err: any) {
            next(err)
            return
        }
        next()
    }

    private async schedulePipeline(record: IRecord, recurssionDepth: number = 0) {
        const log = this.log.getLogger(`Scheduler::schedulePipeline(recDepth = ${recurssionDepth})`)
        log.info(`Handling record: ${record.localId}`)
        log.debug(record)

        const node = this.addNewNode(record.localId)
        const cRecord = cleanRecord(record)
        log.debug(cRecord)
        const updatedRecord = await this.substituteAllPublishedReferences(cRecord)
        node.data('recordJson', updatedRecord)
        await this.tryToPublish(node)
        log.debug('Calling next')
    }

    protected addNewNode(localId: string): cytoscape.CollectionReturnValue {
        const log = this.log.getLogger("Scheduler::addNode")
        const node = this._graph.$id(localId)
        log.debug("Node")
        log.debug(node)
        log.debug(node.isNode())
        if (node.isNode()) {
            log.warn(`This node already exists: ${localId}`)
            return node
        }
        const nodeData: INode = {
            group: 'nodes',
            data: {
                id: localId,
                oipRef: undefined,
                recordJson: undefined
            }
        }
        const newNode = this._graph.add(nodeData)
        return newNode
    }

    private async substituteAllPublishedReferences(record: IRecord): Promise<IRecord> {
        const log = this.log.getLogger("Scheduler::substituteAllPublishedReferences")
        const matches = this.checkDependencies(record)
        let newRecordString = JSON.stringify(record)
        if (matches) {
            for (const reference of matches) {
                log.info(`Found a reference: ${reference}`)
                const pubInfo = await this.isPublished(reference)
                log.debug(pubInfo)
                const oipRef = pubInfo ? pubInfo.oipRef : undefined
                if (oipRef) {
                    newRecordString = newRecordString.replace(reference, oipRef)
                }
            }
        }
        log.info(`Updated record`)
        const updatedRecord = JSON.parse(newRecordString) as IRecord
        log.info(newRecordString)
        return updatedRecord
    }

    protected checkDependencies(record: IRecord) {
        const log = this.log.getLogger("Scheduler::checkDependencies")
        const matchReferences = new RegExp(`${prefix}.*?"`, 'g')
        const newRecord = JSON.parse(JSON.stringify(record))
        delete newRecord['localId']
        let newRecordString = JSON.stringify(newRecord)
        log.debug(newRecordString)
        let deps = matchReferences.exec(newRecordString)
        const matches: string[] = []
        while ( deps !== null ) {
            // This is necessary to avoid infinite loops with zero-width matches
            if (deps.index === matchReferences.lastIndex) {
                matchReferences.lastIndex++;
            }
            
            // The result can be accessed through the `m`-variable.
            deps.forEach((match, groupIndex) => {
                log.debug(`Found match, group ${groupIndex}: ${match}`);
                matches.push(match.slice(0, -1))
            });
            deps = matchReferences.exec(newRecordString)
        }
        log.debug(matches)
        return matches
    }

    private async tryToPublish(node: cytoscape.CollectionReturnValue) {
        const log = this.log.getLogger("Scheduler::tryToPublish")
        if (this.db === undefined) {
            throw Error('This writable must be initialized before piped.')
        }
        const nodeId = node.data('id')
        const nodeHaveBeenPublished = await this.isPublished(nodeId)

        if (this.limit !== 0 && this.limit <= this.stats.published) {
            log.warn('Reached the limit.')
            return
        }

        if (nodeHaveBeenPublished) {
            log.warn(`Node ${nodeId} have been published. Skipping. Delete DB to force publishing all records in the same dataset.`)
            node.data('recordJson', nodeHaveBeenPublished.recordJson)
            node.data('oipRef', nodeHaveBeenPublished.oipRef)
            this.stats.skipped++
            await this.tryPublishingDependents(nodeId, nodeHaveBeenPublished.oipRef)
            return
        }


        log.info(`Trying to publish ${nodeId}`)
        const metadata = node.data('recordJson')
        const pendingDependencies = this.checkDependencies(metadata)
        log.debug(metadata)
        log.debug(pendingDependencies)
        if (pendingDependencies.length > 0) {
            log.info(`There are pending dependencies in this record.`)
            log.info(`Adding its dependencies: ${JSON.stringify(pendingDependencies)}`)
            for (const source of pendingDependencies) {
                const nodeDep = this.addNewNode(source)
                const filtered = this._graph
                    .filter(`[source = "${nodeDep.data().id}"]`)
                log.debug('filtered')
                log.debug(filtered.toArray().map((d) => d.data()))
                if (filtered.toArray().map((d) => d.data().target).includes(nodeId)) continue
                const edge: IEdge = {
                    group: 'edges',
                    data: {
                        source,
                        target: nodeId
                    }
                }
                try {
                    this._graph.add(edge)
                } catch (err) {
                    throw err
                }
            }
        } else {
            try {
                log.debug(`Record ${nodeId} is about to be published `)
                const successPublishedRecordInfo = await this.publishRecord(metadata)
                log.info(`Record published ${successPublishedRecordInfo.oipRef}`)
                this.db.get('publDB').push(successPublishedRecordInfo).value()
                await this.db.write()
                node.data({
                        oipRef: successPublishedRecordInfo.oipRef
                    })
                await this.tryPublishingDependents(nodeId, successPublishedRecordInfo.oipRef)
            } catch (err) {
                throw err
            }
        }
    }

    private async tryPublishingDependents(nodeId: string, oipRef: string) {
        const log = this.log.getLogger("Scheduler::tryToPublishingDependents")
        const filtered = this._graph
            .filter(`[source = "${nodeId}"]`)
        
        for (const edge of filtered.toArray()) {
                log.info(`record connection is:`)
                log.debug(edge.data())
                const target = edge.data('target')
                log.info(`Target: ${target}`)
                if (target)  {
                    const targetNode = this._graph.$id(target)
                    const targetMetadata = targetNode.data('recordJson')
                    const updatedTargetMetadata = JSON.parse(JSON.stringify(targetMetadata).replace(nodeId, oipRef))
                    targetNode.data('recordJson', updatedTargetMetadata)
                    await this.tryToPublish(targetNode)
                }
        }
    }

    private async isPublished(id: string): Promise<IPublDB | undefined> {
        return this.db?.get('publDB').find({id}).value()
    }

    private async publishRecord(record: IRecord): Promise<IPublDB> {
        const log = this.log.getLogger("Scheduler::publishRecord")
        log.info(`Publishing record:`)
        const parser = new ParseRecords(this.loglevel)
        const oipRecordDetails = await parser.encode(record).catch((err) => log.error(err)) // todo: change this for parsing the record.
        const recordRaw = {
            detailsData: oipRecordDetails,
            wif: this.config.owner.wif,
            network: this.config.broadcaster.network
        }
        log.debug(JSON.stringify(recordRaw))
        const oipRecord = recordProtoBuilder(recordRaw)
        log.debug(oipRecord)
        const oipRef = this.pretend ? this.publisher?.fakeFeedBlockchain(oipRecord.signedMessage64) :
            await this.publisher?.feedBlockchain(`p64:${oipRecord.signedMessage64}`)
        if (oipRef === undefined) {
            throw Error('failed to publish record')
        }
        this.stats.published++
        return {
            id: record.localId,
            oipRef,
            recordJson: record
        }
    }
}

export {
    Scheduler
}