import { Transform } from "stream";
import { LogLevelDescType, Logger} from 'loglevel-colored-prefix'
import { prefix, localIdPrefix } from "./definitions";


class UpdateLocalIdTransform extends Transform {
    private readonly name: string
    private readonly log: Logger
    private ids: Set<string>
    private readonly res: {foreign: RegExp, local: RegExp}

    constructor(name: string, loglevel: LogLevelDescType = 'info') {
        super({objectMode: true})
        this.name = name
        this.log = new Logger(loglevel)
        this.ids = new Set()
        this.res  = {
            foreign: new RegExp(prefix, 'g'),
            local: new RegExp(localIdPrefix, 'g')
        }
    }

    _transform(chunkBuffer: any, _env: BufferEncoding, next: any) {
        const log = this.log.getLogger("UpdateLocalIdTransform::Transform")
        const chunk = JSON.parse(chunkBuffer)
        log.debug(chunk)
        const localId = chunk.localId
        if (!localId) {
            const errorMsg = `Cannot find localId column in ${this.name}.csv. All csv files submitted to Ashurbanipal must have a localId column`
            log.error(errorMsg)
            // throw Error(errorMsg)
            next(errorMsg)
        } 
        if (this.ids.has(localId)) {
            const errorMsg = `Duplicated record in ${this.name}.csv.\n \
            Offending record: \n \
            ${JSON.stringify(chunk, null, '')} \n \
            `
            log.error(errorMsg)
            // throw Error(errorMsg)
            next(errorMsg)
        }
        const upLocalId = `${prefix}-${this.name}-${localId}`
        //const upChunkLocalIdResolved = JSON.stringify(chunk).replace(this.res.foreign, `${prefix}-${this.name}`)
        const upChunk = JSON.parse(JSON.stringify(chunk).replace(this.res.local, `${prefix}-${this.name}-`))

        const newRecord ={
            ...upChunk,
            localId: upLocalId
        }
        log.debug(newRecord)
        this.push(newRecord)
        next()
    }

}

export {
    UpdateLocalIdTransform
}