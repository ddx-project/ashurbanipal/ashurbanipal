#!/usr/bin/env node

// tslint:disable: no-console
import fs from 'fs'
import { ArgumentParser } from "argparse";
import chalk from "chalk";
import figlet from "figlet";
import { Logger, LogLevelDescType } from "loglevel-colored-prefix";
import { Ashurbanipal } from './Ashurbanipal';
import { IConfigAshurbanipal } from './interfaces';
// import { IGeneHoodConfig } from "../interfaces";

const logger = new Logger("info");
const log = logger.getLogger("ashurbanipal");

// tslint:disable-next-line: no-var-requires no-require-imports
const pkjson = require("../package.json");

const splash = figlet.textSync("Ashurbanipal", {
  font: "Larry 3D",
  horizontalLayout: "fitted"
});

console.log(chalk.cyan(splash));
console.log(`\t\t\t\t\t\t\t\t  ${chalk.cyan("" + pkjson.version)}`);
console.log(chalk.red("\t\t\t\t\t\t\t by Davi Ortega"));

const parser = new ArgumentParser({
  description:
    "Command line application to bulk publish records on FLO using OIP.",
  prog: "ashurbanipal",
  add_help: true
});


parser.add_argument("--config", {
  help: "Publisher configuration file",
  nargs: 1
});

parser.add_argument("--mode", {
  choices: ["pretend", "publish"],
  help: "Determine the publication mode.",
  nargs: 1,
  required: true
});

parser.add_argument("--records", {
  help:
    "List of csv files with records",
  required: true,
  nargs: '*'
});

parser.add_argument("--logLevel", {
  choices: ["quiet", "verbose"],
  help:
    "Makes Ashurbanipal to be quieter and only log warns and errors or verbose with debug level logs."
});

parser.add_argument("--start-over-from-scratch-i-know-what-i-am-doing", {
  help: "This will erase the internal database of published records and after this, the records will be published again",
  action: "store_true",
  default: false,
  dest: "eraseDB"
});


const args = parser.parse_args();

console.log(args)

const logLevel = !args.logLevel
  ? "info"
  : args.logLevel === "quiet"
  ? "warn"
  : "debug";

const pretend = args.mode[0] === 'pretend'
const eraseDB = args.eraseDB || pretend
console.log(args.mode[0])
console.log(pretend)
console.log(eraseDB)
console.log('---')

const main = async (config: IConfigAshurbanipal) => {
  const ashur = new Ashurbanipal(config, logLevel, pretend, eraseDB);
  ashur.loadCsvFiles(args.records)
  await ashur.init()
  await ashur.publish(pretend)
  if (pretend) {
      const ashurTmp = new Ashurbanipal(config, logLevel, pretend, true);
      ashur.loadCsvFiles(args.records)
      await ashur.init()
  }
  console.log('done')
}

const config = JSON.parse(fs.readFileSync(args.config[0]).toString())
main(config)


/*
if (args.action === "init") {
  genehood.init(args.project);
} else if (args.addPhylogeny || args.addRange || args.addStableIds) {
  if (args.addPhylogeny) {
    const config = JSON.parse(
      fs.readFileSync(`${args.project}.geneHood.config.json`).toString()
    ) as IGeneHoodConfig;
    log.info(`Adding phylogeny.`);
    config.user.newickTree = fs.readFileSync(args.addPhylogeny[0]).toString();
    fs.writeFileSync(
      `${args.project}.geneHood.config.json`,
      JSON.stringify(config, null, " ")
    );
    const packExists = fs.existsSync(`${args.project}.geneHood.pack.json.gz`);
    if (packExists) {
      config.user.startingStep = "buildMockPhylogeny";
      fs.writeFileSync(
        `${args.project}.geneHood.config.json`,
        JSON.stringify(config, null, " ")
      );
      genehood
        .load(args.project)
        .run()
        .catch(err => {
          throw err;
        });
    }
  }
  if (args.addStableIds) {
    log.info(`Adding stable ids from file: ${args.addStableIds[0]}`);
    const stableIds = fs
      .readFileSync(args.addStableIds[0])
      .toString()
      .split("\n")
      .filter(id => id !== "");
    log.info(`Adding ${stableIds.length} ids.`);
    const config = JSON.parse(
      fs.readFileSync(`${args.project}.geneHood.config.json`).toString()
    ) as IGeneHoodConfig;
    config.user.settings.stableIds = stableIds;
    fs.writeFileSync(
      `${args.project}.geneHood.config.json`,
      JSON.stringify(config, null, " ")
    );
  }
  if (args.addRange) {
    const config = JSON.parse(
      fs.readFileSync(`${args.project}.geneHood.config.json`).toString()
    ) as IGeneHoodConfig;
    const downstream = args.addRange[0];
    const upstream = args.addRange[1];
    if (upstream && downstream) {
      config.user.settings.downstream = downstream;
      config.user.settings.upstream = upstream;
      log.info(`Adding new range`);
      log.info(`Downstream: ${downstream}`);
      log.info(`Upstream ${upstream}`);
      fs.writeFileSync(
        `${args.project}.geneHood.config.json`,
        JSON.stringify(config, null, " ")
      );
    } else {
      log.error(`Range must be integer numbers.`);
      log.error(`Downstream: ${downstream}`);
      log.error(`Upstream ${upstream}`);
    }
  }
} else if (args.action === "cleanUp") {
  fs.unlinkSync(`${args.project}.geneHood.data.json.gz`);
  fs.unlinkSync(`${args.project}.geneHood.fasta.temp.fa`);
  fs.unlinkSync(`${args.project}.geneHood.blastp.temp.dat`);
  const blastDbFiles = glob.sync(`${args.project}.ghdb.*`);
  blastDbFiles.forEach(file => {
    fs.unlinkSync(file);
  });
} else {
  const keepGoing = args.action === "keepGoing";
  genehood
    .load(args.project, keepGoing)
    .run()
    .catch(err => {
      throw err;
    });
}
*/