const SIGLIM = 6

const dbPath = `./.data`
const dbFilename = (sig: string) => `${dbPath}/${sig}.ashur.db.json`;
const dbFilenameTest = (sig: string) => `${dbPath}/${sig}.ashur.db.test.json`

const globPatterTestDbs = `${dbPath}/*ashur.db.test.json`

const prefix = `ashuRef`
const localIdPrefix = `localId-`

const oipApiBaseUrl =  "https://api.oip.io/oip/o5/"

export {
    SIGLIM,
    prefix,
    dbPath,
    dbFilename,
    dbFilenameTest,
    globPatterTestDbs,
    localIdPrefix,
    oipApiBaseUrl
}