import fs from 'fs'
import { Readable } from 'stream'

interface IConfigAshurbanipal {
    broadcaster: IConfigBroadcaster
    owner: IConfigOwner
}

interface IConfigBroadcaster {
    network: string
    rpc: IRpcConfig
    publicAddress: string
    wif: string
    txFeePerByte: number
}

interface IRpcConfig {
    port: number
    host: string
    username: string
    password: string
}

interface IConfigOwner {
    // mnemonics: string
    publicAddress: string
    wif: string
}

interface ISource {
    sigStream: Readable
    stream: Readable
    name: string
}

interface IDepeDB {
    id: string,
    lo: string[],
    lb: string[] 
}

interface IPublDB {
    id: string,
    oipRef: string,
    recordJson: object
}

interface IDbSchema {
    // depeDB: IDepeDB[],
    // waitDB: [],
    publDB: IPublDB[]
}

interface IRecord {
    localId: string
    [other: string]: {
        [field: string]: string | string[]
    } | string
}

interface INode {
    group: 'nodes'
    data: {
        id: string
        oipRef: undefined | string
        recordJson: undefined | Object
    }
}

interface IEdge {
    group: 'edges',
    data: {
        source: string,
        target: string
    }
}

interface IDependencies {
    resolved: {
        [value: string]: string
    }
    pending: string[]
}

interface IFileDescriptor {
    name: string,
    descriptor: string
}

interface IOipDetails {
    name: string,
    descriptor: string,
    payload: any
}

interface IStats {
    published: number
    skipped: number
}
export {
    IConfigAshurbanipal,
    IConfigBroadcaster,
    IConfigOwner,
    ISource,
    IDbSchema,
    IDepeDB,
    IPublDB,
    IRecord,
    INode,
    IEdge,
    IDependencies,
    IFileDescriptor,
    IOipDetails,
    IStats
}