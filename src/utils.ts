import { IRecord } from "./interfaces";

const cleanRecord = (record: IRecord): IRecord => {
    for (const template in record) {
        let details = record[template]
        if (typeof details !== "string") {
            if (details['oipRefs']) {
                details['oipRefs'] = (details['oipRefs'] as string[]).filter((d: string) => d !== "")
            }
            for (const key in details) {
                try {
                    details[key] = (details[key] as string[]).filter((d: string) => d !== "")
                }
                catch (err) {
                   //console.error(err)
                    //console.log(details)
                    //console.log(key)
                }
            }
            record[template] = details
        }
    }
    return record
}

export {
    cleanRecord
}