import fs from 'fs'
import glob from 'glob'
import chai from 'chai'
import chaiAsPromised from 'chai-as-promised'
import { LogLevelDescType } from 'loglevel-colored-prefix'

import { sampleConfig } from './fixtures'
import { Ashurbanipal } from '../src/Ashurbanipal'
import { globPatterTestDbs } from '../src/definitions'
import { Readable } from 'stream'

chai.use(chaiAsPromised);
const expect = chai.expect

chai.should()

const loglevel: LogLevelDescType = 'info'

const sampleSimpleCsvFilename = './test/sample.simple.csv'
const sampleWithRefCsvFilename = './test/sample.withRef.csv'
const sampleWithRefCsvFilenameMod = './test/sample.withRef.smallMod.csv'
const sampleSimpleBadCsvFilename = './test/sample.simple.bad.csv'

const sampleCsvFilename = './test/sample.dog.csv'
const sampleCsvFilename2 = './test/sample.cat.csv'

const sampleSelfRefFilename = './test/sample.dog.selfRef.csv'
const sampleCircRefFilename = './test/sample.dog.circRef.csv'
const sampleMissRefFilename = './test/sample.dog.missRef.csv'

const sampleCircRefFilename1 = './test/sample.dog.circRef.2files.csv'
const sampleCircRefFilename2 = './test/sample.cat.circRef.2files.csv'

const sampleLargeFilename = './test/sample.neruda.single.large.csv'

describe("Ashurbanipal", () => {
    describe("parseFilename", () => {
        class Mock extends Ashurbanipal {
            public parseFilename(filename: string): string {
                return super.parseFilename(filename)
            }
        }
        describe("simple filenames", () => {
            it("should parse relative paths", () => {
                const ash = new Mock(sampleConfig)
                const filename = "./path1/path2/sample.csv"
                const parsed = ash.parseFilename(filename)
                expect(parsed).eql("sample")
            })
            it("should parse absolute paths", () => {
                const ash = new Mock(sampleConfig)
                const filename = "/path1/path2/sample.csv"
                const parsed = ash.parseFilename(filename)
                expect(parsed).eql("sample")
            })
            it("should parse just filenames", () => {
                const ash = new Mock(sampleConfig)
                const filename = "sample.csv"
                const parsed = ash.parseFilename(filename)
                expect(parsed).eql("sample")
            })
        })
        describe("filenames with dots", () => {
            it("should parse relative paths", () => {
                const ash = new Mock(sampleConfig)
                const filename = "./path1/path2/sample.oip.42.csv"
                const parsed = ash.parseFilename(filename)
                expect(parsed).eql("sample.oip.42")
            })
            it("should parse absolute paths", () => {
                const ash = new Mock(sampleConfig)
                const filename = "/path1/path2/sample.oip.42.csv"
                const parsed = ash.parseFilename(filename)
                expect(parsed).eql("sample.oip.42")
            })
            it("should parse just filenames", () => {
                const ash = new Mock(sampleConfig)
                const filename = "sample.oip.42.csv"
                const parsed = ash.parseFilename(filename)
                expect(parsed).eql("sample.oip.42")
            })
        })


    })
    describe("init", () => {
        beforeEach(() => {
            try {
                glob(globPatterTestDbs,{},(err, files: string[]) => {
                    files.forEach(file => fs.unlinkSync(file))
                })
            } catch (err) {
                console.log(`No old tests... keep going`)
            }
        })
        it("should throw if no data is loaded before init", async function () {
            const ash = new Ashurbanipal(sampleConfig, loglevel, true)
            return expect(ash.init()).rejectedWith('Must load data before initialize it')
        })
        it("should pass if data pased before init", async function () {
            this.timeout(8000)
            const ash = new Ashurbanipal(sampleConfig, loglevel, true)
            ash.loadCsvFiles([sampleWithRefCsvFilename])
            return expect(ash.init()).fulfilled
        })
    })
    describe("calcSigs", () => {
        class Mock extends Ashurbanipal {
            public async calcSigs(): Promise<string> {
                return await super.calcSigs()
            }
        }
        beforeEach(() => {
            try {
                glob(globPatterTestDbs,{},(err, files: string[]) => {
                    files.forEach(file => fs.unlinkSync(file))
                })
            } catch (err) {
                console.log(`No old tests... keep going`)
            }
        })
        it("should calc a sig from files", async () => {
            const ash = new Mock(sampleConfig, loglevel, true)
            ash.loadCsvFiles([sampleSimpleCsvFilename])
            const sig = await ash.calcSigs()
            expect(sig).eq('8ef327')
        })
        it("should give different sigs when loading slightly different data files", async function () {
            this.timeout(8000)
            
            const ash1 = new Mock(sampleConfig, loglevel, true)
            ash1.loadCsvFiles([sampleWithRefCsvFilename])
            const sig1 = await ash1.calcSigs()

            const ash2 = new Mock(sampleConfig, loglevel, true)
            ash2.loadCsvFiles([sampleWithRefCsvFilenameMod])
            const sig2 = await ash2.calcSigs()
            expect(sig1).to.not.be.equal(sig2)
        })
        it("should give same sig when loading same data files in different order", async () => {
            
            const ash1 = new Mock(sampleConfig, loglevel, true)
            ash1.loadCsvFiles([sampleWithRefCsvFilename, sampleCsvFilename2])
            const sig1 = await ash1.calcSigs()

            const ash2 = new Mock(sampleConfig, loglevel, true)
            ash2.loadCsvFiles([sampleCsvFilename2, sampleWithRefCsvFilename])
            const sig2 = await ash2.calcSigs()
            console.log(sig1)
            console.log(sig2)
            expect(sig1).eq(sig2)
        })
        it("should be integrated with init", async () => {
            
            const ash1 = new Ashurbanipal(sampleConfig, loglevel, true)
            ash1.loadCsvFiles([sampleWithRefCsvFilename])
            const sig1 = await ash1.init().then(async () => {
                return ash1.datasetSig
            })

            const ash2 = new Ashurbanipal(sampleConfig, loglevel, true)
            ash2.loadCsvFiles([sampleCsvFilename2])
            const sig2 = await ash2.init().then(async () => {
                return ash2.datasetSig
            })
            expect(sig1).to.not.be.equal(sig2)
        })
    })
    describe("calcSig", () => {
        class Mock extends Ashurbanipal {
            public async calcSig(stream: Readable): Promise<string> {
                return await super.calcSig(stream)
            }
        }
        it("should calc a sig from a file", async function () {
            const ash = new Mock(sampleConfig, loglevel, true)
            const stream = fs.createReadStream(sampleWithRefCsvFilename)
            const sig = await ash.calcSig(stream)
            expect(sig).eq("26c2cf4fcd171efbef918f22b2b262fc83f43e92487bcb3749f4c65cc1ebb3ae")
        })
    })
    describe("integration", () => {
         beforeEach(() => {
            try {
                glob(globPatterTestDbs,{},(err, files: string[]) => {
                    files.forEach(file => fs.unlinkSync(file))
                })
            } catch (err) {
                console.log(`No old tests... keep going`)
            }
        })
        after(() => {
            try {
                glob(globPatterTestDbs,{},(err, files: string[]) => {
                    files.forEach(file => fs.unlinkSync(file))
                })
            } catch (err) {
                console.log(`No old tests... keep going`)
            }
        })
        it("should work with three simple records", async function() {
            this.timeout(10000)
            const ash = new Ashurbanipal(sampleConfig, loglevel, true)
            const expectedDbState = [
                {
                  id: 'ashuRef-sample.simple-1',
                  oipRef: '1eeebfde8d745ee533f13e4a480b50e8ff42f625242c49cea2b841e59f62e2f9',
                  recordJson: {
                    "localId": "ashuRef-sample.simple-1",
                    "tmpl_0F89BC3B": {
                      "string": "rec1",
                      "bool": true,
                      "bools": [
                        false,
                        false
                      ],
                      "oipRefs": []
                    }
                  }
                },
                {
                  id: 'ashuRef-sample.simple-2',
                  oipRef: '90759d66d70ee53550ce4da91b121718bcb91fda0c3fc316e6b36df70b9947e4',
                  recordJson: {
                    "localId": "ashuRef-sample.simple-2",
                    "tmpl_0F89BC3B": {
                      "string": "rec2",
                      "bool": false,
                      "bools": [],
                      "oipRefs": []
                    }
                  }
                }
            ]
            ash.loadCsvFiles([sampleSimpleCsvFilename])
            await ash.init()
            await ash.publish(true)
            const db = ash.exportDb()
            expect(db).to.eql(expectedDbState)
        })
        it("should throw with broken references", async () => {
            const ash = new Ashurbanipal(sampleConfig, loglevel, true)
            ash.loadCsvFiles([sampleSimpleBadCsvFilename])
            return ash.init().then(() => {
                return ash.publish(true).should.be.rejectedWith(Error)
            })
        })
        it("should work with correct references", async function () {
            this.timeout(8000)
            const ash = new Ashurbanipal(sampleConfig, loglevel, true)
            const orderOfPublishing = [
                "ashuRef-sample.withRef-2",
                "ashuRef-sample.withRef-3",
                "ashuRef-sample.withRef-1"
            ]
            ash.loadCsvFiles([sampleWithRefCsvFilename])
            await ash.init()
            await ash.publish(true)
            const db = ash.exportDb()
            expect(db).to.not.be.undefined
            if (db) {
                expect(db.map(d => d.id)).to.eql(orderOfPublishing)
            }
        })
        it("should work with correct references in different files", async function(){
            this.timeout(10000)
            const ash = new Ashurbanipal(sampleConfig, loglevel, true)
            const orderOfPublishing = [
                "ashuRef-sample.dog-1",
                "ashuRef-sample.cat-3",
                "ashuRef-sample.dog-3",
                "ashuRef-sample.dog-2",
                "ashuRef-sample.cat-2",
                "ashuRef-sample.cat-1",
                "ashuRef-sample.cat-4",
            ]
            ash.loadCsvFiles([sampleCsvFilename, sampleCsvFilename2])
            await ash.init()
            await ash.publish(true)
            const db = ash.exportDb()
            expect(db).to.not.be.undefined
            if (db) {
                expect(db.map(d => d.id)).to.eql(orderOfPublishing)
            }
        })
        it("should work with correct references in different files in both orders", async function(){
            this.timeout(20000)
            const ash = new Ashurbanipal(sampleConfig, loglevel, true)
            const orderOfPublishing = [
                "ashuRef-sample.cat-3",
                "ashuRef-sample.dog-1",
                "ashuRef-sample.dog-3",
                "ashuRef-sample.dog-2",
                "ashuRef-sample.cat-2",
                "ashuRef-sample.cat-1",
                "ashuRef-sample.cat-4",
            ]
            ash.loadCsvFiles([sampleCsvFilename2, sampleCsvFilename])
            await ash.init()
            await ash.publish(true)
            const db = ash.exportDb()
            expect(db).to.not.be.undefined
            if (db) {
                expect(db.map(d => d.id)).to.eql(orderOfPublishing)
            }
        })
        it("should work with large records", async function () {
            this.timeout(8000)
            const ash = new Ashurbanipal(sampleConfig, loglevel, true)
            const orderOfPublishing = [
                "ashuRef-sample.neruda.single.large-1"
            ]
            ash.loadCsvFiles([sampleLargeFilename])
            await ash.init()
            await ash.publish(true)
            const db = ash.exportDb()
            expect(db).to.not.be.undefined
            if (db) {
                expect(db.map(d => d.id)).to.eql(orderOfPublishing)
            }
        })
        it("should not re-publish records from the same dataset", async function() {
            this.timeout(10000)
            const ash = new Ashurbanipal(sampleConfig, loglevel, true)
            ash.loadCsvFiles([sampleSimpleCsvFilename])
            await ash.init()
            await ash.publish(true)
            
            const ash2 = new Ashurbanipal(sampleConfig, loglevel, true)
            ash2.loadCsvFiles([sampleSimpleCsvFilename])
            await ash2.init()
            await ash2.publish(true)
            
            const stats1 = ash.getStats()
            const stats2 = ash2.getStats()
                
            expect(stats1).eql({published: 2, skipped: 0})
            expect(stats2).eql({published: 0, skipped: 2})
            
        })
        it("should erase the DB with eraseDB = true and re-publidh records", async function() {
            this.timeout(10000)
            const ash = new Ashurbanipal(sampleConfig, loglevel, true)

            ash.loadCsvFiles([sampleSimpleCsvFilename])
            await ash.init()
            await ash.publish(true)
            
            const ash2 = new Ashurbanipal(sampleConfig, loglevel, true, true)
            ash2.loadCsvFiles([sampleSimpleCsvFilename])
            await ash2.init()
            await ash2.publish(true)

            const stats1 = ash.getStats()
            const stats2 = ash2.getStats()
            expect(stats1).eql({published: 2, skipped: 0})
            expect(stats2).eql({published: 2, skipped: 0})
        })
        it("should not publish more items than the limit in publisher", async function(){
            this.timeout(20000)
            const ash = new Ashurbanipal(sampleConfig, loglevel, true)
            const orderOfPublishing = [
                "ashuRef-sample.cat-3",
                "ashuRef-sample.dog-1",
            ]
            ash.loadCsvFiles([sampleCsvFilename2, sampleCsvFilename])
            await ash.init()
            await ash.publish(true, 2)
            const db = ash.exportDb()
            expect(db).to.not.be.undefined
            if (db) {
                expect(db.map(d => d.id)).to.eql(orderOfPublishing)
            }
        })
        it("should throw if publish is called twice.", async function(){
            this.timeout(20000)
            const ash = new Ashurbanipal(sampleConfig, loglevel, true)
            const orderOfPublishing = [
                "ashuRef-sample.cat-3",
                "ashuRef-sample.dog-1",
                "ashuRef-sample.dog-3",
                "ashuRef-sample.dog-2",
                "ashuRef-sample.cat-2",
            ]
            ash.loadCsvFiles([sampleCsvFilename2, sampleCsvFilename])
            await ash.init()
            await ash.publish(true, 2)
            expect(ash.publish(true, 3)).rejectedWith("There are no more data source. If you are trying to publish more records, you must start a new instance and load the files again.")
        })
        it("should publish more items if restarted.", async function(){
            this.timeout(20000)
            const ash = new Ashurbanipal(sampleConfig, loglevel, true)
            const orderOfPublishing = [
                "ashuRef-sample.cat-3",
                "ashuRef-sample.dog-1",
                "ashuRef-sample.dog-3",
                "ashuRef-sample.dog-2",
                "ashuRef-sample.cat-2",
            ]
            ash.loadCsvFiles([sampleCsvFilename2, sampleCsvFilename])
            await ash.init()
            await ash.publish(true, 2)
            const ash2 = new Ashurbanipal(sampleConfig, loglevel, true)
            ash2.loadCsvFiles([sampleCsvFilename2, sampleCsvFilename])
            await ash2.init()
            await ash2.publish(true, 3)
            const db = ash2.exportDb()
            expect(db).to.not.be.undefined
            if (db) {
                expect(db.map(d => d.id)).to.eql(orderOfPublishing)
            }
        })
        it("should publish the rest of the records if asked to", async function(){
            this.timeout(20000)
            const ash = new Ashurbanipal(sampleConfig, loglevel, true)
            const orderOfPublishing = [
                "ashuRef-sample.cat-3",
                "ashuRef-sample.dog-1",
                "ashuRef-sample.dog-3",
                "ashuRef-sample.dog-2",
                "ashuRef-sample.cat-2",
                "ashuRef-sample.cat-1",
                "ashuRef-sample.cat-4",
            ]
            ash.loadCsvFiles([sampleCsvFilename2, sampleCsvFilename])
            await ash.init()
            await ash.publish(true, 2)
            const ash2 = new Ashurbanipal(sampleConfig, loglevel, true)
            ash2.loadCsvFiles([sampleCsvFilename2, sampleCsvFilename])
            await ash2.init()
            await ash2.publish(true, 3)
            const ash3 = new Ashurbanipal(sampleConfig, loglevel, true)
            ash3.loadCsvFiles([sampleCsvFilename2, sampleCsvFilename])
            await ash3.init()
            await ash3.publish(true)
            const db = ash3.exportDb()
            expect(db).to.not.be.undefined
            if (db) {
                expect(db.map(d => d.id)).to.eql(orderOfPublishing)
            }
        })
    })
    describe("edge cases", () => {
        beforeEach(() => {
            try {
                glob(globPatterTestDbs,{},(err, files: string[]) => {
                    files.forEach(file => fs.unlinkSync(file))
                })
            } catch (err) {
                console.log(`No old tests... keep going`)
            }
        })
        after(() => {
            try {
                glob(globPatterTestDbs,{},(err, files: string[]) => {
                    files.forEach(file => fs.unlinkSync(file))
                })
            } catch (err) {
                console.log(`No old tests... keep going`)
            }
        })
        it('should be rejected with self-reference', async () => {
            const ash = new Ashurbanipal(sampleConfig, loglevel, true)
            ash.loadCsvFiles([sampleSelfRefFilename])
            await ash.init()
            return expect(ash.publish(true)).rejectedWith(Error) 
        })
        it('should be rejected with circular reference', async () => {
            const ash = new Ashurbanipal(sampleConfig, loglevel, true)
            ash.loadCsvFiles([sampleCircRefFilename])
            await ash.init()
            return expect(ash.publish(true)).rejectedWith(Error) 
        })
        it('should be rejected with circular reference between 2 files', async () => {
            const ash = new Ashurbanipal(sampleConfig, loglevel, true)
            ash.loadCsvFiles([sampleCircRefFilename1, sampleCircRefFilename2])
            await ash.init()
            return expect(ash.publish(true)).rejectedWith(Error) 
        })
        it('should be rejected with missing reference', async () => {
            const ash = new Ashurbanipal(sampleConfig, loglevel, true)
            ash.loadCsvFiles([sampleMissRefFilename])
            await ash.init()
            return expect(ash.publish(true)).rejectedWith(Error) 
        })
    })
    describe("test integration with blockchain", () => {
        beforeEach(() => {
            try {
                glob(globPatterTestDbs,{},(err, files: string[]) => {
                    files.forEach(file => fs.unlinkSync(file))
                })
            } catch (err) {
                console.log(`No old tests... keep going`)
            }
        })
        after(() => {
            try {
                glob(globPatterTestDbs,{},(err, files: string[]) => {
                    files.forEach(file => fs.unlinkSync(file))
                })
            } catch (err) {
                console.log(`No old tests... keep going`)
            }
        })
        it("should work", async function(){
            this.timeout(20000)
            const ash = new Ashurbanipal(sampleConfig, loglevel, true)
            const orderOfPublishing = [
                "ashuRef-sample.cat-3",
                "ashuRef-sample.dog-1",
            ]
            ash.loadCsvFiles([sampleCsvFilename2, sampleCsvFilename])
            await ash.init()
            await ash.publish(false, 2)
            const db = ash.exportDb()
            expect(db).to.not.be.undefined
            if (db) {
                expect(db.map(d => d.id)).to.eql(orderOfPublishing)
            }
        })
        it("should work with large records", async function () {
            this.timeout(8000)
            const ash = new Ashurbanipal(sampleConfig,loglevel, true)
            const orderOfPublishing = [
                "ashuRef-sample.neruda.single.large-1"
            ]
            ash.loadCsvFiles([sampleLargeFilename])
            await ash.init()
            await ash.publish()
            const db = ash.exportDb()
            expect(db).to.not.be.undefined
            if (db) {
                expect(db.map(d => d.id)).to.eql(orderOfPublishing)
            }
        })
    })
})