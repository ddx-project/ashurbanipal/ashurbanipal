import { expect } from 'chai'
import { LogLevelDescType } from 'loglevel-colored-prefix'
import { IConfigAshurbanipal, IConfigOwner } from '../src/interfaces'
import { ParseRecords } from '../src/ParseRecords'

const logLevel: LogLevelDescType = "debug"


const fixtures = {
    templateResponse: {
        "count": 1,
        "total": 1,
        "results": [
         {
          "template": {
           "identifier": 260684859,
           "friendly_name": "test template",
           "file_descriptor_set": "CosGCgdwLnByb3RvEhJvaXBQcm90by50ZW1wbGF0ZXMi4wUKAVASDgoGc3RyaW5nGAEgASgJEgwKBGJvb2wYAiABKAgSDQoFYnl0ZXMYAyABKAwSDgoGZG91YmxlGAQgASgBEg0KBWZsb2F0GAUgASgCEhIKBGVudW0YBiABKA4yBEVudW0SFAoGb2lwUmVmGAcgASgLMgRUeGlkEg0KBWludDMyGAggASgFEg0KBWludDY0GAkgASgDEg4KBnVpbnQzMhgKIAEoDRIOCgZ1aW50NjQYCyABKAQSDgoGc2ludDMyGAwgASgREg4KBnNpbnQ2NBgNIAEoEhIPCgdmaXhlZDMyGA4gASgHEg8KB2ZpeGVkNjQYDyABKAYSEAoIc2ZpeGVkMzIYECABKA8SEAoIc2ZpeGVkNjQYESABKBASDwoHc3RyaW5ncxgSIAMoCRINCgVib29scxgTIAMoCBIOCgZieXRlc3MYFCADKAwSDwoHZG91YmxlcxgVIAMoARIOCgZmbG9hdHMYFiADKAISFAoFZW51bXMYFyADKA4yBUVudW1zEhUKB29pcFJlZnMYGCADKAsyBFR4aWQSDgoGaW50MzJTGBkgAygFEg4KBmludDY0UxgaIAMoAxIPCgd1aW50MzJTGBsgAygNEg8KB3VpbnQ2NFMYHCADKAQSDwoHc2ludDMyUxgdIAMoERIPCgdzaW50NjRTGB4gAygSEhAKCGZpeGVkMzJTGB8gAygHEhAKCGZpeGVkNjRTGCAgAygGEhEKCXNmaXhlZDMyUxghIAMoDxIRCglzZml4ZWQ2NFMYIiADKBAaEwoEVHhpZBILCgNyYXcYASABKAwiNgoERW51bRISCg5FbnVtX1VOREVGSU5FRBAAEgwKCEVudW1fT05FEAESDAoIRW51bV9UV08QAiJLCgVFbnVtcxITCg9FbnVtc19VTkRFRklORUQQABINCglFbnVtc19PTkUQARINCglFbnVtc19UV08QAhIPCgtFbnVtc19USFJFRRADYgZwcm90bzM=",
           "name": "tmpl_0F89BC3B",
           "description": "A comprehensive test template designed by the DDX team"
          },
          "meta": {
           "signed_by": "FB41shJxHWphSMp2kG9atqSZ4yWVqUWsaH",
           "block_hash": "d80ad0f749b0f595164f8255ab4bb410b7345f011717d143ac147e454767ccbd",
           "txid": "0f89bc3bf3addded2c21b2f93b77da235ba95e45e2b41213d5d01fa6f06af3c1",
           "block": 4719300,
           "time": 1617803570
          }
         }
        ],
        "next": "%5B1617803570%5D",
        "descriptor": "CosGCgdwLnByb3RvEhJvaXBQcm90by50ZW1wbGF0ZXMi4wUKAVASDgoGc3RyaW5nGAEgASgJEgwKBGJvb2wYAiABKAgSDQoFYnl0ZXMYAyABKAwSDgoGZG91YmxlGAQgASgBEg0KBWZsb2F0GAUgASgCEhIKBGVudW0YBiABKA4yBEVudW0SFAoGb2lwUmVmGAcgASgLMgRUeGlkEg0KBWludDMyGAggASgFEg0KBWludDY0GAkgASgDEg4KBnVpbnQzMhgKIAEoDRIOCgZ1aW50NjQYCyABKAQSDgoGc2ludDMyGAwgASgREg4KBnNpbnQ2NBgNIAEoEhIPCgdmaXhlZDMyGA4gASgHEg8KB2ZpeGVkNjQYDyABKAYSEAoIc2ZpeGVkMzIYECABKA8SEAoIc2ZpeGVkNjQYESABKBASDwoHc3RyaW5ncxgSIAMoCRINCgVib29scxgTIAMoCBIOCgZieXRlc3MYFCADKAwSDwoHZG91YmxlcxgVIAMoARIOCgZmbG9hdHMYFiADKAISFAoFZW51bXMYFyADKA4yBUVudW1zEhUKB29pcFJlZnMYGCADKAsyBFR4aWQSDgoGaW50MzJTGBkgAygFEg4KBmludDY0UxgaIAMoAxIPCgd1aW50MzJTGBsgAygNEg8KB3VpbnQ2NFMYHCADKAQSDwoHc2ludDMyUxgdIAMoERIPCgdzaW50NjRTGB4gAygSEhAKCGZpeGVkMzJTGB8gAygHEhAKCGZpeGVkNjRTGCAgAygGEhEKCXNmaXhlZDMyUxghIAMoDxIRCglzZml4ZWQ2NFMYIiADKBAaEwoEVHhpZBILCgNyYXcYASABKAwiNgoERW51bRISCg5FbnVtX1VOREVGSU5FRBAAEgwKCEVudW1fT05FEAESDAoIRW51bV9UV08QAiJLCgVFbnVtcxITCg9FbnVtc19VTkRFRklORUQQABINCglFbnVtc19PTkUQARINCglFbnVtc19UV08QAhIPCgtFbnVtc19USFJFRRADYgZwcm90bzM="
       }
}


describe("ParseRecords", () => {
    describe("getTemplate", () => {
        class Mock extends ParseRecords {
            public getTemplate(localId: string) {
                return super.getTemplate(localId)
            }
/*             public updateNode(record: IRecord) {
                return super.updateNode(record)
            } */
        }
        it('should work', async function() {
            const templateId = "tmpl_0F89BC3B"
            const parser = new Mock(logLevel)
            const template = await parser.getTemplate(templateId)
            expect(template).eql(fixtures.templateResponse)
        })
    })
    describe("encode", () => {
        it('should work', async () => {
            const recordRaw = {
                localId: "ashuRef-sample.dog-3",
                tmpl_0F89BC3B: {
                  string: "pitoco",
                  oipRefs: [
                    "c3ef42cf930aee73c8d522ea9f805bd80d8674232306fdaebaf723033962985e"
                  ]
                }
            }
            const parser = new ParseRecords(logLevel)
            const record = await parser.encode(recordRaw)
            console.log(record)
        })
    })

})