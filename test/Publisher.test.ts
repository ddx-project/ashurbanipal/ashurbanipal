import { expect } from 'chai'
import { LogLevelDescType } from 'loglevel-colored-prefix'
import { IConfigAshurbanipal, IConfigOwner } from '../src/interfaces'
import { Publisher } from '../src/Publisher'

const logLevel: LogLevelDescType = "debug"

const masterFixture = {
    mnemonic: "gentle maximum smooth analyst concert leopard way sheriff piano hurdle love wheel",
    wifs: [ "R8kEAGsg4AMp2BTjEfhDd9pyLN6UPk8EKDVqhFF694QXP7tzjmCz" ],
    pubKeys: [
        "FHhiFfEzUxf7bEP5o1A4mzzA5ma5tHTNV3"
    ] 
}
      
const sampleConfig: IConfigAshurbanipal = {
    broadcaster: {
        network: "main",
        rpc: {
            port: 7313,
            host: "127.0.0.1",
            username: "username",
            password: "password"
        },
        publicAddress: "FFb1em1ZBE6BzLihSCxscqW6KLkLtpWzBH",
        wif: "RAh4VD17ckTAqHRQ6QXpYBUXL3UGakoubyeB3unuTzvPBEnGZJ1A",
        txFeePerByte: 0.0000001
    },
    owner: {
        wif: masterFixture.wifs[0],
        // mnemonics: masterFixture.mnemonic,
        publicAddress: masterFixture.pubKeys[0]
    }
}

const testTemplate = {
    name: "tmpl_0F89BC3B",
    id: 260684859
}

describe('Publisher', () => {
    describe('init', () => {
        it('should initialize the configuration and be ready', () => {
            const publisher = new Publisher(sampleConfig)
            publisher.init().then(pup => {
                expect(pup.isReady()).to.be.true
            })
        })
    })
    describe('feedBlockchain', () => {
        it('it should work once',async function () {
            this.timeout(120000)
            const mnemonic = masterFixture.mnemonic
            const publisher = new Publisher(sampleConfig)
            await publisher.init()
            const txId = await publisher.feedBlockchain(`Ashurbanipal single test`)
            expect(txId).to.have.lengthOf(64)
        })
        it('it should work more than once',async function () {
            this.timeout(120000)
            const mnemonic = masterFixture.mnemonic
            const publisher = new Publisher(sampleConfig)
            await publisher.init()
            const txIds1 = await publisher.feedBlockchain(`Ashurbanipal multi test 1/3`)
            expect(txIds1).to.have.lengthOf(64)
            const txIds2 = await publisher.feedBlockchain(`Ashurbanipal multi test 2/3`)
            expect(txIds2).to.have.lengthOf(64)
            const txIds3 = await publisher.feedBlockchain(`Ashurbanipal multi test 3/3`)
            expect(txIds3).to.have.lengthOf(64)
        })
        it.skip('it should work once with large record',async function () {
            this.timeout(120000)
            const mnemonic = masterFixture.mnemonic
            const publisher = new Publisher(sampleConfig)
            await publisher.init()
            const txId = await publisher.feedBlockchain(`Ashurbanipal single test`)
            expect(txId).to.have.lengthOf(1)
        })
    })
})