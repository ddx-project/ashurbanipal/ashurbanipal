import fs from 'fs'
import { expect } from 'chai'
import { LogLevelDescType } from 'loglevel-colored-prefix'
import { sampleConfig } from './fixtures'

import { Scheduler } from '../src/Scheduler'
import { Readable, Writable } from 'stream'
import { IRecord } from '../src/interfaces'
import { globPatterTestDbs } from '../src/definitions'
import glob from 'glob'

const loglevel: LogLevelDescType = 'debug'

const sampleRecords = JSON.parse(fs.readFileSync('./test/sampleRecords.json').toString())


describe("Scheduler", () => {
    beforeEach(() => {
        try {
            glob(globPatterTestDbs,{},(err, files: string[]) => {
                files.forEach(file => fs.unlinkSync(file))
            })
        } catch (err) {
            console.log(`No old tests... keep going`)
        }
    })
    describe("Unit tests", () => {
        class Mock extends Scheduler {
            public addNewNode(localId: string) {
                return super.addNewNode(localId)
            }
            public checkDependencies(record: IRecord) {
                return super.checkDependencies(record)
            }
/*             public updateNode(record: IRecord) {
                return super.updateNode(record)
            } */
        }
        describe("addNewNode", () => {
            it("should add node to graph", async () => {
                const scheduler = new Mock(sampleConfig, loglevel, true)
                await scheduler.init('test')
                const localId: string = "ashuRef-cats-1"
                const expected = {
                    "id": "ashuRef-cats-1",
                    "oipRef": undefined,
                    "recordJson": undefined
                }
                
                const node = scheduler.addNewNode(localId)
                expect(node.data()).to.eql(expected)
            })
            it("should return existing node if try to add with existing id", async () => {
                const scheduler = new Mock(sampleConfig, loglevel, true)
                await scheduler.init('test')
                const localId: string = "ashuRef-cats-1"
                const expected = {
                    "id": "ashuRef-cats-1",
                    "oipRef": undefined,
                    "recordJson": undefined
                }
                scheduler.addNewNode(localId)
                const existingNode = scheduler.addNewNode(localId)
                expect(existingNode.data()).to.eql(expected)
            })
        })
        describe("checkDependencies", () => {
            it("should note a dependency", async () => {
                const scheduler = new Mock(sampleConfig, loglevel, true)
                await scheduler.init('test')
                const record: IRecord = {
                    "localId": "ashuRef-sample.withRef-1",
                    "tmpl_0F89BC3B": {
                        "string": "rec1",
                        "bool": "true",
                        "bools": [
                          "false",
                          "false"
                        ],
                        "oipRefs": [
                          "ashuRef-sample.withRef-3"
                        ]
                    }
                }
                const expected = ["ashuRef-sample.withRef-3"]
                const dependencies = scheduler.checkDependencies(record)
                expect(dependencies).to.exist
                if (dependencies) {
                    expect(dependencies).to.eql(expected)
                }
            })
            it("should note more than 1 dependencies", async () => {
                const scheduler = new Mock(sampleConfig, loglevel, true)
                await scheduler.init('test')
                const record: IRecord = {
                    "localId": "ashuRef-sample.withRef-1",
                    "tmpl_0F89BC3B": {
                        "string": "rec1",
                        "bool": "true",
                        "bools": [
                          "false",
                          "false"
                        ],
                        "oipRefs": [
                          "ashuRef-sample.dog-1",
                          "ashuRef-sample.cat-2"
                        ]
                    }
                }
                const expected = ["ashuRef-sample.dog-1", "ashuRef-sample.cat-2"]
                const dependencies = scheduler.checkDependencies(record)
                console.log('HEY')
                console.log(dependencies)
                expect(dependencies).to.exist
                if (dependencies) {
                    expect(dependencies).to.eql(expected)
                }
            })
        })
        /*describe("updateNode", () => {
            it("should update existing node to graph", async () => {
                const scheduler = new Mock(sampleConfig, loglevel, true)
                await scheduler.init('test')
                const record: IRecord = {
                    "localId": "ashuRef-sample.withRef-1",
                }
                const updatedRecord: IRecord = {
                    "localId": "ashuRef-sample.withRef-1",
                    "tmpl_0F89BC3B": {
                      "string": "rec1"
                    }
                }
                const expected = {
                    "id":"ashuRef-sample.withRef-1",
                    "oipRef": undefined,
                    "recordJson": {
                        "tmpl_0F89BC3B": {
                            "string":"rec1"
                        }
                    }
                }
                scheduler.addNewNode(record)
                const node = scheduler.updateNode(updatedRecord)
                expect((node.json() as any).data).to.eql(expected)
            })
            it.skip("should throw if try to update node with not empty recordJson. Consider as duplication", async () => {
                const scheduler = new Mock(sampleConfig, loglevel, true)
                await scheduler.init('test')
                const record: IRecord = {
                    "localId": "ashuRef-sample.withRef-1",
                    "tmpl_0F89BC3B": {
                      "string": "rec1",
                      "bool": "true",
                      "bools": [
                        "false",
                        "false"
                      ],
                      "oipRefs": [
                        "ashuRef-sample.withRef-3"
                      ]
                    }
                }
                scheduler.addNewNode(record)
                expect(function () { scheduler.addNewNode(record)}).to.throw(`This node already exists`)
            })
        })*/
    })
    describe.skip("Transform", () => {
        describe("should work", () => {
            it.skip("should throw if not connected with DB first", async () => {
                /*const readable = new Readable({objectMode: true})
                sampleRecords.forEach((record: any) => {
                    readable.push(record)
                })
                readable.push(null)
                const scheduler = new Scheduler(sampleConfig, true, loglevel)
                readable.pipe(scheduler)
                scheduler.on('data', (data) => {
                    console.log(`Scheduling data: ${data.localId}`)
                })
                scheduler.on('error', (err) => {
                    return expect(err.message).eql("TThis writable must be initialized.")
                })
                scheduler.on('end', () => {
                    return expect(1, "The stream should have thrown an Error").eql(0)
                }) */
            })
            it("should parse relative paths", async () => {
                const scheduler = new Scheduler(sampleConfig, loglevel)
                const readable = new Readable({objectMode: true})
                sampleRecords.forEach((record: any) => {
                    readable.push(record)
                })
                readable.push(null)
                await scheduler.init('test')
                scheduler.pretend = true
                readable.pipe(scheduler)
                scheduler.on('data', (data) => {
                    console.log(`Scheduling data: ${data.localId}`)
                })
                scheduler.on('error', (err) => {
                    scheduler.cork()
                    throw err
                })
                scheduler.on('finish', () => {
                    console.log("end")
                    return expect(1, "The stream should have thrown an Error").eql(0)
                })
                // expect(parsed).eql("sample")
            })
        })
    })
})