import { IConfigAshurbanipal } from "../src/interfaces"

const masterFixture = {
    mnemonic: "gentle maximum smooth analyst concert leopard way sheriff piano hurdle love wheel",
    wifs: [ "R8kEAGsg4AMp2BTjEfhDd9pyLN6UPk8EKDVqhFF694QXP7tzjmCz" ],
    pubKeys: [
        "FHhiFfEzUxf7bEP5o1A4mzzA5ma5tHTNV3"
    ] 
}

const sampleConfig: IConfigAshurbanipal = {
    broadcaster: {
        network: "mainnet",
        rpc: {
            port: 7313,
            host: "127.0.0.1",
            username: "username",
            password: "password"
        },
        publicAddress: "FFb1em1ZBE6BzLihSCxscqW6KLkLtpWzBH",
        wif: "RAh4VD17ckTAqHRQ6QXpYBUXL3UGakoubyeB3unuTzvPBEnGZJ1A",
        txFeePerByte: 0.0001 / 1024
    },
    owner: {
        wif:"R8kEAGsg4AMp2BTjEfhDd9pyLN6UPk8EKDVqhFF694QXP7tzjmCz",
        publicAddress: masterFixture.pubKeys[0]
    }
}

export {
    masterFixture,
    sampleConfig
}